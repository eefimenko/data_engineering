import json
import sys
import datetime, time
import numpy as np

# To find median numpy version is used
def median(array):
    return np.median(np.array(array))

# Read in parameters as input and output files
input_file = sys.argv[1]
output_file = sys.argv[2]

# Prepare data and output file 
content = open(input_file, 'r')
result = open(output_file, 'w')
# list containing actual transactions
# during iteration we add parsed transaction and remove all transactions
# outside of the sliding window. This list contains information about
# transactions that might be removed upon next move of sliding window
transactions = []
# dictionary containing graph information in form: name: number of edges
# during update names with zero number of edges are removed
vertices = {}
# time related to "youngest" transaction, based on this time the 60s
# sliding window is evaluated
max_time = 0.

# Read in json data using json library and process it line-by-line
for line in content:
    transaction = json.loads(line)
    # convert created time to datetime structure for future processing
    t = datetime.datetime.strptime(transaction['created_time'], '%Y-%m-%dT%H:%M:%SZ')
    # convert time to seconds since the epoch for maintaining the 60s sliding window
    tsec = time.mktime(t.timetuple())
    # We do not need exact date and time, so simply replace created_time with time in seconds
    transaction['created_time'] = tsec
    
    # if transaction falls within the 60s sliding window, process it
    # in this case sliding_median is updated and saved to file
    # otherwise the transaction is skipped and sliding_median from the previous step is saved
    if tsec > max_time - 60:
        # place this transaction into the list of all transactions
        transactions.append(transaction)
        # Add transaction to the dictionary with names and value representing number of edges
        # If name is not in dictionary just create it with value 1 as 1 edge now exists
        # If name is already in list just add 1 , as one more edge is created
        if transaction['target'] in vertices:
            vertices[transaction['target']] += 1.
        else:
            vertices[transaction['target']] = 1.
        if transaction['actor'] in vertices:
            vertices[transaction['actor']] += 1.
        else:
            vertices[transaction['actor']] = 1.
        
        # check if need to update max time    
        if tsec > max_time:
            max_time = tsec
        # Check if we need to remove some older transactions
        # first add them to auxilary list
        trans_to_remove = []
        for transaction in transactions:
            if transaction['created_time'] < max_time - 60:
                trans_to_remove.append(transaction)
                
        # remove transactions from the actual transaction list
        # after that update the graph information by removing edges 
        for transaction in trans_to_remove:
            vertices[transaction['target']] -= 1.
            vertices[transaction['actor']] -= 1. 
            transactions.remove(transaction)
        
        # remove all names with 0 number of edges
        for k in vertices.keys():
            if vertices[k] == 0:
                del vertices[k]
            
        # Now extract number of edges for all names   
        values = vertices.values()
        # Calculate median for this array and store this value to output file
        sliding_median = median(values)
    # Note, that if transaction is skipped in case it is outside the sliding window
    # the last calculated is stored (because in this case graph does not change) 
    result.write("%.2lf\n" %(sliding_median))
# Finalize: close files    
content.close()
result.close()

