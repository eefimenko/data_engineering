#!/usr/bin/env bash

# example of the run script for running the rolling_median calculation with a python file, 
# but could be replaced with similar files from any major language

# I'll execute my programs, with the input directory data_input and output the files in the directory data_output
python ./src/rolling_median.py ./data_input/data-trans.txt ./data_output/output.txt



